#!/usr/bin/env node
import prompts from "prompts";
import * as API from "./API";
import { Spinner } from "cli-spinner";
import * as util from "./util";
import { HELP, HELP_VISKA } from "./docs";
import { Command } from "./util";
import os from "os";
import fs from "fs";
import { getConfig, CONFIG_FILE } from "./util";

interface Config extends API.AuthReponse {
  defaultGroup: string;
}

/**
 * Entry point. Performs basic checks before delegating to one of the command function with config and command line args as parameters
 */
const run = async () => {
  const args = process.argv.slice(2);
  if (!args || args.length === 0) {
    console.log(HELP_VISKA);
    process.exit(0);
  }
  const command = args[0];
  const params = args.slice(1);
  if (!fs.existsSync(CONFIG_FILE) && command !== "init") {
    console.log(
      `Config file not found at ${CONFIG_FILE}. Please run ${util.colorCode(
        "viska init"
      )}`
    ),
      process.exit(1);
  }
  const config = await getConfig();
  if (args[1] === "--help" || args[1] === "-h") {
    helpCommand(config, [command]);
    process.exit(0);
  }
  switch (command) {
    case "init":
      await initCommand(config, params);
      break;
    case "config":
      await configCommand(config, params);
      break;
    case "get":
      await getCommand(config, params);
      break;
    case "add":
      await addCommand(config, params);
      break;
    case "update":
      await updateCommand(config, params);
      break;
    case "delete":
      await deleteCommand(config, params);
      break;
    case "ls":
      await lsCommand(config, params);
      break;
    case "--help":
    case "-help":
    case "-h":
      console.log(HELP_VISKA);
      break;
    case "help":
      helpCommand(config, params);
      break;
    case "-v":
    case "-version":
    case "--version":
      console.log("Viska version 0.1");
      break;
    default:
      console.log(
        util.colorError("Unknown command"),
        util.colorCode(command),
        util.colorError(". Run 'viska -help' for available commands")
      );
      break;
  }
  process.exit(0);
};

const initCommand = async (config: Config, params: string[]) => {
  // only interested in side-effect (logging error and warning on input validation), not return value
  util.parseParams(
    {
      requiredParams: [],
      optedParams: [],
    },
    params
  );
  const response = await prompts([
    {
      type: "text",
      name: "username",
      message: `Username (${os.userInfo().username}):`,
    },
    {
      type: "password",
      name: "password",
      message: `Password:`,
    },
  ]);
  const username = response.username || os.userInfo().username;
  const password = response.password;
  const spinner = new Spinner(`Authenticating.. %s`);
  spinner.setSpinnerString("|/-\\");
  spinner.setSpinnerDelay(200);
  spinner.start();
  const jwt = await API.authenticate(username, password);
  spinner.stop(true);
  if (!jwt) {
    console.log(`Authenticating ${util.colorError(" ...failed!")}`);
    process.exit(1);
  }
  console.log(`Authenticating ${util.colorSuccess(" ...success!")}`);
  const defaultGroup = await util.promptDefaultGroup(jwt.groups);
  const newConfig: Config = {
    ...jwt,
    defaultGroup,
  };
  util.updateConfig(newConfig, "Configuration saved. Viska init complete!");
};

const helpCommand = (config: Config, params: string[]) => {
  if (params.length === 0) {
    console.log(HELP.help);
  } else {
    const helpText = HELP[params[0] as Command];
    console.log(helpText);
  }
  process.exit(0);
};

const configCommand = async (config: Config, params: string[]) => {
  if (params.length === 0) {
    helpCommand(config, ["config"]);
  }
  const subcommand = params[0];
  switch (subcommand) {
    case "ls":
      util.printConfig(config);
      break;
    case "set-group":
      const defaultGroup = await util.promptDefaultGroup(config.groups);
      const newConfig: Config = {
        ...config,
        defaultGroup,
      };
      util.updateConfig(newConfig, "Default group updated!");
      break;
    case "reset":
      await initCommand(config, params);
      break;
    case "destroy-session":
      config.jwt = "";
      config.exp = 0;
      fs.writeFileSync(CONFIG_FILE, JSON.stringify(config), "utf8");
      console.log("Session destroyed!");
      break;
    default:
      console.log(
        util.colorError(
          "Usage: viska config [ls | set-group | reset | destroy-session]"
        )
      );
      process.exit(1);
  }
  process.exit(0);
};

const getCommand = async (config: Config, params: string[]) => {
  if (params.length === 0) {
    helpCommand(config, ["get"]);
  }
  const jwt = await util.resolveAuth(config);
  const parsedParams = util.parseParams(
    {
      requiredParams: ["key"],
      optedParams: [
        { name: "group", argReq: "ONE", default: config.defaultGroup },
        { name: "verbose", argReq: "NONE", default: "false" },
      ],
    },
    params,
    (x: string) => {
      switch (x) {
        case "g":
          return "group";
        case "v":
          return "verbose";
        default:
          return x;
      }
    }
  );
  util.validateGroup(config, parsedParams.optedParams.group[0]);
  const response = await API.getSecrets(
    parsedParams.requiredParams.key,
    parsedParams.optedParams.group[0],
    jwt
  );
  switch (response.statusCode) {
    case 200:
      util.printSecret(
        response.data,
        parsedParams.optedParams.verbose[0] === "true"
      );
      break;
    case 403:
      console.log(`You are not authorized to view this resource`);
      break;
    case 404:
      console.log(
        `No matching secret found, use ${util.colorCode(
          "viska ls"
        )} to search for secrets`
      );
      break;
    case 500:
      console.log(`Something went wrong. Please try again`);
  }
  process.exit(0);
};

const addCommand = async (config: Config, params: string[]) => {
  if (params.length === 0) {
    helpCommand(config, ["add"]);
  }
  const jwt = await util.resolveAuth(config);
  const parsedParams = util.parseParams(
    {
      requiredParams: ["key", "value"],
      optedParams: [
        { name: "tags", argReq: "MORE THAN NONE", default: undefined },
        { name: "group", argReq: "ONE", default: config.defaultGroup },
        { name: "notes", argReq: "ONE", default: "" },
        { name: "lifespan", argReq: "ONE", default: undefined },
      ],
    },
    params,
    (value: string) => {
      switch (value) {
        case "t":
          return "tags";
        case "g":
          return "group";
        case "n":
          return "notes";
        case "l":
          return "lifespan";
      }
      return value;
    }
  );
  const currentGroup = parsedParams.optedParams.group
    ? parsedParams.optedParams.group[0]
    : config.defaultGroup;
  if (!config.groups.includes(currentGroup)) {
    console.log(
      util.colorError(
        "Error: '" +
          currentGroup +
          "' is not a valid group. List available groups with"
      ),
      util.colorCode("viska config ls")
    );
    process.exit(1);
  }
  const result = await API.postSecret(
    parsedParams.requiredParams.key,
    parsedParams.requiredParams.value,
    parsedParams.optedParams.notes[0],
    currentGroup,
    parsedParams.optedParams.tags[0] === undefined
      ? []
      : parsedParams.optedParams.tags,
    jwt
  );
  switch (result.statusCode) {
    case 200:
      console.log(
        util.colorSuccess("Secret added for '" + currentGroup + "'!")
      );
      process.exit(0);
    case 409:
      console.log(
        util.colorError(
          `The secret ${util.colorHighlight(
            parsedParams.requiredParams.key
          )} already exist in the group ${util.colorHighlight(
            currentGroup
          )}. Use ${util.colorCode(
            "viska update"
          )} to update an existing secret`
        )
      );
      process.exit(1);
    case 500:
      console.log(
        util.colorError(
          "Something went wrong. Please try again later or contact IMS for support"
        )
      );
      process.exit(1);
  }
};

const deleteCommand = async (config: Config, params: string[]) => {
  if (params.length === 0) {
    helpCommand(config, ["delete"]);
  }
  const jwt = await util.resolveAuth(config);
  const parsedParams = util.parseParams(
    {
      requiredParams: ["key"],
      optedParams: [
        { name: "group", argReq: "ONE", default: config.defaultGroup },
      ],
    },
    params,
    (value: string) => {
      switch (value) {
        case "g":
          return "group";
      }
      return value;
    }
  );
  util.validateGroup(config, parsedParams.optedParams.group[0]);

  const answer = await prompts({
    type: "text",
    name: "confirmation",
    message: `Deleting ${util.colorHighlight(
      parsedParams.requiredParams.key)
    } in ${util.colorHighlight(
      parsedParams.optedParams.group[0]
    )}. Confirm by typing ${util.colorHighlight(
      parsedParams.requiredParams.key)
    }. To abort, leave blank:`,
    validate: (value: string) =>
      value.toLowerCase() === parsedParams.requiredParams.key.toLowerCase() || !value
        ? true
        : `Confirm by typing ${util.colorHighlight(
          parsedParams.requiredParams.key)
        }. To abort, leave blank`,
  });
  if (answer.confirmation.toLowerCase() !== parsedParams.requiredParams.key.toLowerCase() ) {
    console.log(util.colorError("Aborting"));
    process.exit(0);
  }
  const result = await API.deleteSecret(
    parsedParams.requiredParams.key,
    parsedParams.optedParams.group[0],
    jwt
  );
  if (result.statusCode === 200) {
    console.log(util.colorSuccess("Secret deleted!"));
  } else {  
    console.log(
      util.colorError(
        "Something went wrong and the secret could not be deleted"
      )
    );
  }
};
const updateCommand = async (config: Config, params: string[]) => {
  if (params.length === 0) {
    helpCommand(config, ["update"]);
  }
  const jwt = await util.resolveAuth(config);
  const parsedParams = util.parseParams(
    {
      requiredParams: ["key"],
      optedParams: [
        { name: "value", argReq: "ONE", default: undefined },
        { name: "tags", argReq: "MORE THAN NONE", default: undefined },
        { name: "group", argReq: "ONE", default: config.defaultGroup },
        { name: "notes", argReq: "ONE", default: undefined },
        { name: "replace-tags", argReq: "NONE", default: "false" },
      ],
    },
    params,
    (value: string) => {
      switch (value) {
        case "v":
          return "value";
        case "t":
          return "tags";
        case "g":
          return "group";
        case "n":
          return "notes";
        case "r":
          return "replace-tags";
      }
      return value;
    }
  );
  util.validateGroup(config, parsedParams.optedParams.group[0]);
  const result = await API.patchSecret(
    parsedParams.requiredParams.key,
    parsedParams.optedParams.value[0],
    parsedParams.optedParams.notes[0],
    parsedParams.optedParams.group[0],
    parsedParams.optedParams.tags[0] === undefined
      ? []
      : parsedParams.optedParams.tags,
    parsedParams.optedParams["replace-tags"][0] === "true",
    jwt
  );
  if (result.statusCode === 200) {
    console.log(util.colorSuccess("Secret updated!"));
  } else {
    console.log(
      util.colorError(
        "Something went wrong and the secret could not be updated"
      )
    );
  }
};

const lsCommand = async (config: Config, params: string[]) => {
  const jwt = await util.resolveAuth(config);
  const parsedParams = util.parseParams(
    {
      requiredParams: [],
      optedParams: [
        { name: "key", argReq: "ONE", default: undefined },
        { name: "tag", argReq: "ONE", default: undefined },
        { name: "notes", argReq: "ONE", default: undefined },
        { name: "group", argReq: "ONE", default: config.defaultGroup },
        { name: "verbose", argReq: "NONE", default: "false" },
        { name: "all", argReq: "NONE", default: "false" },
        { name: "match-case-key", argReq: "NONE", default: "false" },
        { name: "match-case-tag", argReq: "NONE", default: "false" },
      ],
    },
    params,
    (value: string) => {
      switch (value) {
        case "k":
          return "key";
        case "t":
          return "tag";
        case "n":
          return "notes";
        case "g":
          return "group";
        case "a":
          return "all";
        case "v":
          return "verbose";
        case "mck":
          return "match-case-key";
        case "mct":
          return "match-case-tag";
      }
      return value;
    }
  );
  const group = parsedParams.optedParams.group[0];
  const verbose = parsedParams.optedParams.verbose[0] === "true";
  const all = parsedParams.optedParams.all[0] === "true";
  util.validateGroup(config, group);
  const response = await API.search({
    key: parsedParams.optedParams.key[0],
    tag: parsedParams.optedParams.tag[0],
    notes: parsedParams.optedParams.notes[0],
    group: all ? undefined : group,
    matchCaseKey: parsedParams.optedParams["match-case-key"][0] === "true",
    matchCaseTag: parsedParams.optedParams["match-case-tag"][0] === "true",
    jwt,
  });
  if (response.statusCode === 200) {
    await util.printSecrets(response.data, verbose, group, all);
  } else {
    console.log(
      util.colorError("A search could not be performed, please try again")
    );
  }
};

run();
