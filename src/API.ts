import fetch from "node-fetch";
import dotenv from "dotenv";
dotenv.config();
type StatusCode = 200 | 403 | 404 | 409 | 500;
interface APIResponse<T> {
  statusCode: StatusCode;
  data: T;
}
export interface Secret {
  key: string;
  value: string;
  created: Date;
  createdBy: string;
  updated?: Date;
  updatedBy?: string;
  group: string;
  tags: string[];
  notes: string;
}
export interface AuthReponse {
  jwt: string;
  username: string;
  groups: string[];
  phone: string;
  email: string;
  name: string;
  iat: number;
  exp: number;
}

export const VISKA_SERVER_URL = process.env.VISKA_SERVER_URL || "https://viska-server.maxiv.lu.se"
export const authenticate = async (
  username: string,
  password: string
): Promise<AuthReponse | undefined> => {
  if (VISKA_SERVER_URL !== "https://viska-server.maxiv.lu.se"){
    console.log(`[${VISKA_SERVER_URL}]`)
   }
  try {
    const result = await fetch(`${VISKA_SERVER_URL}/auth`, {
      method: "POST",
      body: JSON.stringify({
        username,
        password,
        includeDecoded: true,
      }),
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
    });
    if (!result.ok) {
      return undefined;
    }
    return await result.json();
  } catch (err) {
    console.log("error", err);
    return undefined;
  }
};

export const getSecrets = async (
  key: string,
  group: string,
  jwt: string
): Promise<APIResponse<Secret | undefined>> => {
  const groupParam = group ? `&group=${group}` : "";
  try {
    const result = await fetch(
      `${VISKA_SERVER_URL}/secrets?jwt=${jwt}&key=${key}${groupParam}`,
      {
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
      }
    );
    if (!result.ok) {
      return { statusCode: result.status as StatusCode, data: undefined };
    }
    const data = (await result.json()) as Secret;
    return { statusCode: result.status as StatusCode, data };
  } catch (err) {
    console.log("error patching secret", err);
    return { statusCode: 500, data: undefined };
  }
};

export const patchSecret = async (
  key: string,
  value: string | undefined,
  notes: string | undefined,
  group: string,
  tags: string[],
  replaceTags: boolean,
  jwt: string
): Promise<APIResponse<Secret | undefined>> => {
  try {
    const result = await fetch(`${VISKA_SERVER_URL}/secrets`, {
      method: "PATCH",
      body: JSON.stringify({
        key,
        value,
        notes,
        group,
        tags,
        replaceTags,
        jwt,
      }),
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
    });
    if (!result.ok) {
      return { statusCode: result.status as StatusCode, data: undefined };
    }
    const data = (await result.json()) as Secret;
    return { statusCode: result.status as StatusCode, data };
  } catch (err) {
    console.log("error patching secret", err);
    return { statusCode: 500, data: undefined };
  }
};

export const deleteSecret = async (
  key: string,
  group: string,
  jwt: string
): Promise<APIResponse<undefined>> => {
  try {
    const result = await fetch(`${VISKA_SERVER_URL}/secrets`, {
      method: "DELETE",
      body: JSON.stringify({
        key,
        group,
        jwt,
      }),
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
    });
    return { statusCode: result.status as StatusCode, data: undefined };
  } catch (err) {
    return { statusCode: 500, data: undefined };
  }
};
export const postSecret = async (
  key: string,
  value: string,
  notes: string,
  group: string,
  tags: string[],
  jwt: string
): Promise<APIResponse<Secret | undefined>> => {
  try {
    const result = await fetch(`${VISKA_SERVER_URL}/secrets`, {
      method: "POST",
      body: JSON.stringify({
        key,
        value,
        notes,
        group,
        tags,
        jwt,
      }),
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
    });
    if (!result.ok) {
      return { statusCode: result.status as StatusCode, data: undefined };
    }
    const data = (await result.json()) as Secret;
    return { statusCode: result.status as StatusCode, data };
  } catch (err) {
    return { statusCode: 500, data: undefined };
  }
};
export const search = async (args: {
  key: string;
  tag: string;
  notes: string;
  group: string;
  matchCaseKey: boolean;
  matchCaseTag: boolean;
  jwt: string;
}): Promise<APIResponse<Secret[] | undefined>> => {
  const sb:string[] = []
  for (const [key, value] of Object.entries(args)) {
    if (value !== undefined){
      sb.push(`${key}=${value}`)
    }
  }
  const urlParams = sb.join("&");
  try {
    const result = await fetch(
      `${VISKA_SERVER_URL}/secrets/search?${urlParams}`,
      {
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
      }
    );
    if (!result.ok) {
      return { statusCode: result.status as StatusCode, data: undefined };
    }
    const data = (await result.json()) as Secret[];
    return { statusCode: result.status as StatusCode, data };
  } catch (err) {
    console.log("error patching secret", err);
    return { statusCode: 500, data: undefined };
  }
};
