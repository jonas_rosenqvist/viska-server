import os from "os";
import fs from "fs";
import path from "path";
import prompts from "prompts";
import * as API from "./API";
import { Spinner } from "cli-spinner";
import moment from "moment";
import chalk from "chalk";
export const RECOMMENDED_GROUPS = [
  "Information Management",
  "KITS",
  "account-admins",
  "Kubernetes",
  "Staff",
];
export const CONFIG_FILE = path.join(os.homedir(), ".viskaConfig.json");

interface AuthReponse {
  jwt: string;
  username: string;
  groups: string[];
  phone: string;
  email: string;
  name: string;
  iat: number;
  exp: number;
}

export type Command =
  | "init"
  | "add"
  | "update"
  | "delete"
  | "get"
  | "ls"
  | "config"
  | "help";
interface Config extends AuthReponse {
  defaultGroup: string;
}

export const colorCode = (text: string) => chalk.yellow(text);
export const colorDim = (text: string) => chalk.gray(text);
export const colorHighlight = (text: string) => chalk.magentaBright(text);
export const colorError = (text: string) => chalk.redBright(text);
export const colorSuccess = (text: string) => chalk.greenBright.bold(text);

export const docHeader = (title: string, sub: string, usage: string) => {
  return `
${chalk.bold.cyanBright("• " + title + " •")}
${chalk.gray(getRow("-"))}
${subtitle(sub)}

Usage: ${colorCode(usage)}
`;
};

export const docFooter = () =>
  chalk.italic(`
Note: any value that contain whitespaces  must be enclosed in quotation marks, e.g. ${colorCode(
    '-param "parameter value"'
  )}`);

export const subtitle = (text: string) => chalk.italic(text);
export const getConfig = async (): Promise<Config> => {
  try {
    return JSON.parse(await fs.promises.readFile(CONFIG_FILE, "utf8"));
  } catch (err) {
    return undefined;
  }
};
/**
 * Returns an object with a key for each flag in args (e.g. -all), where the value is a string[] with the parameters for that flag, if any.
 */
export const mapFlags = (args: string[]): { [key: string]: string[] } => {
  const result: { [key: string]: string[] } = {};
  let currentFlag = "";
  args.forEach((e) => {
    if (e.startsWith("-")) {
      currentFlag = e.slice(1).toLowerCase();
      result[currentFlag] = [];
    } else {
      result[currentFlag].push(e);
    }
  });
  return result;
};

export const printSecrets = async (
  secrets: API.Secret[],
  verbose: boolean,
  group: string,
  all: boolean
) => {
  if (secrets.length === 0) {
    console.log(
      chalk.italic(
        `No matching secret found ` +
          (all ? "in any of your groups" : `for ${colorHighlight(group)}`)
      )
    );
    process.exit(0);
  }
  // warn/prompt if we're showing a lot of verbose responses
  if (secrets.length > 10 && verbose) {
    const answer = await prompts({
      type: "text",
      name: "answer",
      message: `Your ${colorCode("ls")} matches ${
        secrets.length
      } secrets. Are you sure you want the ${colorCode(
        "verbose"
      )} version? (yes/no):`,
      validate: (value: string) =>
        value.toLowerCase().startsWith("y") ||
        value.toLowerCase().startsWith("n")
          ? true
          : `Please respond 'Yes' or 'No'`,
    });
    if (answer.answer.toLowerCase().startsWith("n")) {
      verbose = false;
    }
  }
  if (verbose) {
    console.log(getRow("-") + "\n");
    secrets.forEach((secret) => printSecret(secret, verbose, true));
    console.log("\n" + getRow("-"));
  } else {
    console.log(
      colorHighlight(secrets.map((secret) => secret.key).join(" • "))
    );
  }
  if (secrets.length === 100) {
    console.log(chalk.gray.italic("Only showing the first 100 results."));
  } else {
    console.log(chalk.gray.italic(`Displaying ${secrets.length} matches`));
  }
};

export const printSecret = (
  secret: API.Secret,
  verbose: boolean,
  hideSecret: boolean = false
) => {
  if (!verbose) {
    console.log(
      `${chalk.bold(secret.key)} ${chalk.magenta.bgMagenta(secret.value)}`
    );
    return;
  }
  const { group, notes, tags, created, createdBy, updated, updatedBy } = secret;
  const tagList = tags
    .map((tag) => chalk.whiteBright.bold.bgMagenta(` ${tag} `))
    .join(" ");
  console.log(`\n${chalk.bold.underline(secret.key)}
  `);
  if (!hideSecret) {
    console.log(
      `${colorDim("VALUE")}\t${chalk.magenta.bgMagenta(secret.value)}`
    );
  }
  console.log(`${colorDim("GROUP")}\t${colorHighlight(group)}`);
  console.log(
    `${colorDim("CREATED")}\t${colorHighlight(
      moment(new Date(created)).format("YYYY-MM-DD HH:mm") + ` by ${createdBy}`
    )}`
  );

  console.log(
    `${colorDim("UPDATED")}\t${colorHighlight(
      updated && updatedBy
        ? moment(new Date(updated)).format("YYYY-MM-DD HH:mm") +
            ` by ${updatedBy}`
        : ""
    )}`
  );
  console.log(`${colorDim("TAGS")}\t${tagList}`);
  const printedNotes = notes ? colorHighlight(notes) : "";
  console.log(`${colorDim("NOTES")}\t${printedNotes}`);
};

/**
 * Checks if jwt in config is valid. If not, promopts for password, performs a new auth, and saved new jwt to config
 * @param config
 * @returns a new auth token string (JWT)
 */
export const resolveAuth = async (config: Config): Promise<string> => {
  const exp: Date = new Date((config && config.exp * 1000 - 1000) || 0);
  if (exp > new Date()) {
    return config.jwt;
  }
  if (exp < new Date()) {
    console.log(
      `Authentication expired. Please re-authenticate (${config.username})`
    );
    const response = await prompts({
      type: "password",
      name: "password",
      message: `Password:`,
    });
    const password = response.password;
    const spinner = new Spinner(`Authenticating as ${config.username}... %s`);
    spinner.setSpinnerString("|/-\\");
    spinner.setSpinnerDelay(200);
    spinner.start();
    const authResponse = await API.authenticate(config.username, password);
    spinner.stop(true);
    if (!authResponse) {
      console.log(
        `Authenticating as ${config.username}${colorError(" ...failed!")}`
      );
      process.exit(1);
    }
    console.log(
      `Authenticating as ${config.username}${colorSuccess(" ...success!")}`
    );
    const newConfig: Config = {
      ...authResponse,
      defaultGroup: config.defaultGroup,
    };
    console.log(
      chalk.italic.gray(
        `Created 5 min session. Invalidate with ${colorCode(
          "viska config destroy-session"
        )}`
      )
    );
    fs.writeFileSync(CONFIG_FILE, JSON.stringify(newConfig), "utf8");

    return authResponse.jwt;
  }
};

export const updateConfig = (newConfig: Config, msg: string) => {
  fs.writeFileSync(CONFIG_FILE, JSON.stringify(newConfig), "utf8");
  console.log(colorSuccess(msg));
};

export const promptDefaultGroup = async (groups: string[]) => {
  console.log(
    `Select a default group (this can be changed later with ${colorCode(
      "viska config set-group"
    )})`
  );
  const groupList = `${groups
    .map((group, index) =>
      RECOMMENDED_GROUPS.includes(group)
        ? `(${index + 1})\t${group}
      `
        : chalk.gray(`(${index + 1})\t${group}
      `)
    )
    .join("")}
    Select group (1- ${groups.length}):`;
  const groupResp = await prompts({
    type: "number",
    name: "defaultGroup",
    message: `Available groups:
      ${groupList}`,
    validate: (value: number) =>
      value < 1 || value > groups.length
        ? `Please type a value between 1 and ${groups.length}`
        : true,
  });
  return groups[groupResp.defaultGroup - 1];
};
export const printConfig = (config: Config) => {
  console.log(`
${colorCode("USERNAME")}\t${colorHighlight(config.username)}
${colorCode("DEFAULT GROUP")}\t${colorHighlight(config.defaultGroup)}
${colorCode("SESSION EXP.")}\t${colorHighlight(
    moment(new Date(config.exp * 1000)).format("YYYY-MM-DD HH:mm") +
      " (" +
      moment(new Date(config.exp * 1000)).from(moment()) +
      ")"
  )}

${chalk.underline("JWT DATA")}

${colorCode("NAME")}\t\t${config.name}
${colorCode("EMAIL")}\t\t${config.email}
${colorCode("PHONE")}\t\t${config.phone}
${colorCode("GROUPS")}\t\t${config.groups
    .map((group, index) =>
      group === config.defaultGroup ? colorHighlight(group) : group
    )
    .join("\n\t\t")}
`);
};
export const getRow = (character: string) => {
  return character.repeat(process.stdout.columns);
};
interface CommandParams {
  requiredParams: string[];
  optedParams: {
    name: string;
    argReq: "NONE" | "ONE" | "MORE THAN NONE" | "ANY";
    default: string | undefined;
  }[];
}
interface StructuredParams {
  requiredParams: { [paramName: string]: string };
  optedParams: { [paramName: string]: string[] | undefined };
}
export const parseParams = (
  expectedParams: CommandParams,
  actualParams: string[],
  translator?: (actualArg: string) => string
): StructuredParams => {
  const result: StructuredParams = {
    requiredParams: {},
    optedParams: {},
  };
  const translate = translator ? translator : (x: string) => x;
  const normalizedActualParams = actualParams.map((param) => {
    if (param.startsWith("-")) {
      return "-" + translate(param.slice(1));
    }
    return param;
  });
  // requiredParams
  if (normalizedActualParams.length < expectedParams.requiredParams.length) {
    console.log(
      colorError(
        `Error! Required parameters: ${expectedParams.requiredParams
          .map((param) => colorHighlight(param))
          .join(", ")}`
      )
    );
    process.exit(1);
  }
  expectedParams.requiredParams.forEach((param, index) => {
    const actual = normalizedActualParams[index];
    if (actual.startsWith("-")) {
      console.log(
        colorError(
          `Error! Expected ${colorHighlight(param)} but found ${colorHighlight(
            actual
          )}`
        )
      );
      process.exit(1);
    }
    result.requiredParams[param] = actual;
  });

  // optedParams
  if (
    normalizedActualParams.length > expectedParams.requiredParams.length &&
    !normalizedActualParams[expectedParams.requiredParams.length].startsWith(
      "-"
    )
  ) {
    console.log(
      colorError("Error: Unexpected argument"),
      colorCode(normalizedActualParams[expectedParams.requiredParams.length])
    );
    process.exit(1);
  }
  result.optedParams = mapFlags(
    normalizedActualParams.slice(expectedParams.requiredParams.length)
  );
  // warn about ignored opted params
  Object.keys(result.optedParams).forEach((key) => {
    if (
      !expectedParams.optedParams
        .map((op) => op.name.toLowerCase())
        .includes(key)
    ) {
      console.log(`Warning: ignoring unkown option ${colorHighlight(key)}`);
    }
  });
  expectedParams.optedParams.forEach((param) => {
    const actual = result.optedParams[param.name.toLowerCase()];
    const argReq = param.argReq;
    if (actual) {
      // exists
      switch (argReq) {
        case "ANY":
          break;
        case "MORE THAN NONE":
          if (actual.length === 0) {
            console.log(
              colorError(
                `Error! ${colorCode(param.name)} requires at least 1 parameter`
              )
            );
            process.exit(1);
          }
          break;
        case "NONE":
          if (actual.length > 0) {
            console.log(
              colorError(`Error! ${colorCode(param.name)} has no parameters`)
            );
            process.exit(1);
          }
          break;
        case "ONE":
          if (actual.length !== 1) {
            console.log(
              colorError(
                `Error! ${colorCode(param.name)} requires exactly 1 parameter`
              )
            );
            process.exit(1);
          }
          break;
      }
    } else {
      const defaultValue: string | undefined =
        expectedParams.optedParams.filter((p) => p.name === param.name)[0]
          .default;
      result.optedParams[param.name.toLowerCase()] = defaultValue
        ? [defaultValue]
        : [undefined];
    }
  });

  // insert a array of length one with value "true" for arg-less flags to indicate that they're present
  Object.keys(result.optedParams).forEach((key: string) => {
    if (
      result.optedParams[key] !== undefined &&
      result.optedParams[key].length === 0
    ) {
      result.optedParams[key] = ["true"];
    }
  });
  // console.log("Parsed params:", result);
  return result;
};

export const validateGroup = (config: Config, group: string) => {
  if (!config.groups.includes(group)) {
    console.log(
      colorError(
        `Error: ${colorHighlight(
          group
        )} is either not a valid group, or a group you are not a member of. List available groups with ${colorCode(
          "viska config ls"
        )}`
      )
    );
    process.exit(1);
  }
};
