import * as util from "./util";
import chalk from "chalk";

// VISKA
export const HELP_VISKA = `
${util.docHeader(
  "VISKA",
  "A tool for managing and sharing passwords within and across groups",
  "viska COMMAND [ARGS...] [FLAGS...]"
)}
${chalk.underline("COMMANDS")}

${util.colorCode("init")}\t\tIntializes viska on the system
${util.colorCode("add")}\t\tCreates a new secret
${util.colorCode("update")}\t\tUpdates an existing secret
${util.colorCode("delete")}\t\tDeletes an existing secret
${util.colorCode("get")}\t\tFetches an existing secret
${util.colorCode("ls")}\t\tList secrets by key and/or tag
${util.colorCode("config")}\t\tView or update the configuration of viska
${util.colorCode(
  "help"
)}\t\tGet additional information about above commands
${util.docFooter()}
`;

// VISKA
export const HELP_HELP = `
${util.docHeader(
  "HELP",
  "",
  "viska HELP SUBCOMMAND"
)}
${chalk.underline("SUBCOMMANDS")}

${util.colorCode("init")}\t\tIntializes viska on the system
${util.colorCode("add")}\t\tCreates a new secret
${util.colorCode("update")}\t\tUpdates an existing secret
${util.colorCode("delete")}\t\tDeletes an existing secret
${util.colorCode("get")}\t\tFetches an existing secret
${util.colorCode("ls")}\t\tList secrets by key and/or tag
${util.colorCode("config")}\t\tView or update the configuration of viska
${util.colorCode(
  "help"
)}\t\tGet additional information about above commands
${util.docFooter()}
`;

// INIT
const HELP_INIT = `
${util.docHeader(
  "VISKA INIT",
  "Initialized viska on the system by launching an interactive configuration prompt. The initialization creates a file with default configuration including username (for auth.maxiv.lu.se authentication) and default group",
  "viska init"
)}
${util.docFooter()}
`;

// ADD
const HELP_ADD = `
${util.docHeader(
  "VISKA ADD",
  "Adds a secret to the database",
  "viska add KEY VALUE [-tags TAGS...] [-group GROUP] [-notes NOTES] [FLAGS...]"
)}
${chalk.underline("PARAMETERS")}

${util.colorCode("KEY")}\t\tThe name of the secret
${util.colorCode("VALUE")}\t\tThe value of the secret
${util.colorCode(
  "TAGS"
)}\t\tOptional list of tags to categorize the secret.
${util.colorCode(
  "GROUP"
)}\t\tThe group the secret should be shared with. Defaults to ${util.colorCode(
  "defaultGroup"
)}
${util.colorCode("NOTES")}\t\tA description or additional notes

${chalk.underline("FLAGS")}

${util.colorCode(
  "-lifespan T"
)}\tThe secret will only persist for ${util.colorCode(
  "T"
)} minutes (not implemented yet)
${util.docFooter()}
`;

// UPDATE
const HELP_UPDATE = `
${util.docHeader(
  "VISKA UPDATE",
  `Updates a secret based on its key and group. Of omitted, ${util.colorCode(
    "defaultGroup"
  )} is used`,
  "viska update KEY [-value VALUE] [-tags TAGS...] [-group GROUP] [-notes NOTES] [FLAGS...]"
)}
${chalk.underline("PARAMETERS")}

${util.colorCode("KEY")}\t\tThe name of the secret to be updated
${util.colorCode("VALUE")}\t\tThe updated value of the secret
${util.colorCode(
  "TAGS"
)}\t\tUpdated tags for the secret. Note: if a tag contains a whitespace it
\t\tmust be enclosed in quotation marks, e.g. ${util.colorCode('"my tag"')}
${util.colorCode(
  "GROUP"
)}\t\tThe group the secret should be shared with. Defaults to ${util.colorCode(
  "defaultGroup"
)}
${util.colorCode(
  "NOTES"
)}\t\tUpdated notes. If present, will overwrite existing notes

${chalk.underline("FLAGS")}

${util.colorCode(
  "-replace-tags"
)}\tReplace tags rather than appending ${util.colorCode(
  "TAGS"
)} to the existing list.
\t\tUsing this flag while omitting ${util.colorCode(
  "-tags"
)} will delete all tags
${util.docFooter()}
`;

// DELETE
const HELP_DELETE = `
${util.docHeader(
  "VISKA DELETE",
  `Deletes a secret based on its key and group.`,
  "viska delete KEY [-group GROUP]"
)}
`;

// GET
const HELP_GET = `
${util.docHeader(
  "VISKA GET",
  `Fetches a secret based on its key and group. To list existing secrets, use ${util.colorCode("viska ls")}`,
  "viska get KEY [-group GROUP] [FLAGS...]"
)}
${chalk.underline("PARAMETERS")}

${util.colorCode("KEY")}\t\tThe name of the secret to get
${util.colorCode(
  "GROUP"
)}\t\tThe group the secret belongs to. Defaults to ${util.colorCode(
  "defaultGroup"
)}.

${chalk.underline("FLAGS")}

${util.colorCode(
  "-verbose"
)}\tInclude all secret data in the response, such as tag and group information
${util.docFooter()}
`;

// LS
const HELP_LS = `
${util.docHeader(
  "VISKA LS",
  "List secrets based on key or tags.",
  "viska ls [-key KEY] [-tag TAG] [-notes NOTES] [-group GROUP] [FLAGS...]"
)}
${chalk.underline("PARAMETERS")}

${util.colorCode("KEY")}\t\t\tWhole or part of the key to perform a case-insensitive match
\t\t\twith, unless ${util.colorCode("-match-case-key")} is set
${util.colorCode("TAG")}\t\t\tOnly show results for a specific tag. Match is case-insensitive
\t\t\tunless ${util.colorCode("-match-case-tag")} is set
${util.colorCode("NOTES")}\t\t\tCase-insensitive full text search on notes
${util.colorCode(
  "GROUP"
)}\t\t\tThe group the secret belongs to. Defaults to ${util.colorCode(
  "defaultGroup"
)}
\t\t\tunless ${util.colorCode("-all")} is set.

${chalk.underline("FLAGS")}

${util.colorCode("-verbose")}\t\tInclude metadata in the response
${util.colorCode("-all")}\t\t\tSearch across all available groups
${util.colorCode("-match-case-key")}\t\tRequire case to match in ${util.colorCode("KEY")}
${util.colorCode("-match-case-tag")}\t\tRequire case to match in ${util.colorCode("TAGS")}
${util.docFooter()}
`;

// CONFIG
const HELP_CONFIG = `
${util.docHeader(
  "VISKA CONFIG",
  `View or edit parts of the viska configuration (${util.CONFIG_FILE})`,
  "viska config SUBCOMMAND [ARGS...]"
)}
${chalk.underline("SUBCOMMANDS")}

${util.colorCode("ls")}\t\tList the current configuration
${util.colorCode("destroy-session")}\tDestroys the current five minute session if it exists. This will require
\t\tre-authentication the next time viska is used.
${util.colorCode(
  "set-group"
)}\tLists all available groups and an option to select a new default group
${util.colorCode("reset")}\t\tReconfigures viska, identical to ${util.colorCode(
  "viska init"
)}
  ${util.docFooter()}
`;

// TYPES
export const HELP: { [K in util.Command]: string } = {
  init: HELP_INIT,
  get: HELP_GET,
  ls: HELP_LS,
  add: HELP_ADD,
  update: HELP_UPDATE,
  delete: HELP_DELETE,
  config: HELP_CONFIG,
  help: HELP_HELP,
};
