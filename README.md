# cli for viska
## Requirements
Node (tested with v16.0.0) and npm (tested with 7.10.0). npm generally comes bundled with node, which can be install through a [package manager](https://nodejs.org/en/download/package-manager/) or [downloadable executables](https://nodejs.org/en/download/current/)
## Installation
`sudo npm i -g --production https://gitlab.maxiv.lu.se/jonros/viska-cli/-/raw/master/viska-0.1.0.tgz`

After the installation is finished, you should be able to run `viska` in a terminal. If running on Windows, you might have to add an npm bin directory to your `PATH` to make it globally accessible. You can get this directory with `npm config get prefix` or `npm list -g`, which should resolve to something like `C:\Users\username\AppData\Roaming\npm`. This can then be appended to your path with `set PATH=%PATH%;C:\Users\username\AppData\Roaming\npm`

## Usage
Example using `init`, `add` and `get`:

`λ viska init`
```
√ Username (jonas): ... jonros
√ Password: ... ****************
Authenticating  ...success!
Select a default group (this can be changed later with viska config set-group)
√ Available groups:
      (1)       20170251-group
      (2)       ims-web
      (3)       Users
      (4)       w-jonros-pc-0 Administrators
      (5)       Splunk-User
      (6)       account-admins
      (7)       Kubernetes
      (8)       Information Management
      (9)       KITS
      (10)      Staff
      (11)      vpn-blue
      (12)      vpn-green
      (13)      vpn-white

    Select group (1- 13): ... 8
Configuration saved. Viska init complete!
```
`λ viska add myDbName myDbPassword -tags mongodb "some other tag" -notes "For authenticating to the database myDbName on w-v-mongo-0"`
```
Authentication expired. Please re-authenticate (jonros)
√ Password: ... ****************
Authenticating as jonros ...success!
Created 5 min session. Invalidate with viska config destroy-session
Secret added for 'Information Management'!
```

`λ viska get myDbName -verbose`
```
VALUE   myDbPassword
GROUP   Information Management
CREATED 2021-05-04 16:40 by Jonas Rosenqvist
UPDATED
TAGS    mongodb some other tag
NOTES   For authenticating to the database myDbName on w-v-mongo-0
```
## Running in dev mode
```
git clone git@gitlab.maxiv.lu.se:jonros/viska-cli.git
cd viska-cli
npm install 
npm start
```
## Building npm package
```
cd viska-cli
npm pack
```
